+++
title = "writing"
template = "writing.html"
draft = true
+++
## master thesis ##

In my master thesis, I calculate the étale cohomology (with torsion coefficients) of Fargues-Scholze's stack \\( \mathrm{Bun}_G \\) for \\( G = \mathrm{GL}_2 \\).
More precisely, I can show that the inclusion \\(\mathrm{Bun}_2^{\mathrm{ss}} \hookrightarrow \mathrm{Bun}_2 \\) of the semi-stable locus induces an isomorphism on étale cohomology. 
This calculation involves a proof that the dualizing complex \\( R(\mathrm{Bun}_2 \to \mathrm{Spd}(\overline{\mathbb{F}}_q))^{!}\Lambda\\) is given just by \\( \Lambda \\) itself.
I am currently trying to remove the errors in a previous version, and will upload a new one as soon as this is done.

