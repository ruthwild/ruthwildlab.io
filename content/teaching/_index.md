+++
title = "teaching"
template = "miscindex.html"
+++

**algebraic geometry I (winter 2024/25)**

Course Website:  [↗][ag1]

OLAT: [↗][olat]

Time: Tue, 16-18

Location: room 308

---
_exercises_

Working together on the exercises is strongly encouraged!

- [Sheet 0 (no submission)][0] 
 
- [Sheet 0.5 (no submission)][0.5] 

- [Sheet 1 (due 23. October)][1] 

- [Sheet 2 (due 30. October)][2] 

- [Sheet 3 (due 6. November)][3] 

- [Sheet 4 (due 13. November)][4] 

- [Sheet 5 (due 20. November)][5] 

- [Sheet 6 (due 27. November)][6] 

- [Sheet 7 (due 4. December)][7] 

- [Sheet 8 (due 11. December)][8]

- [Sheet 9 (due 18. December)][9]

- [Sheet 10 (due 16. January)][10]

- [Sheet 11 (due 23. January)][11]

- [Sheet 12 (due 29. January)][12]

- [Sheet 13 (due 5. February)][13]

- [all true-or-false questions][tf] 


---
_announcements_

<details>
<summary>
<em>Welcome</em>
</summary>
Welcome to the class!
I’m looking forward to spending the term with you and the material. 
Everything you find here is also available on OLAT.
</details>
<br>
<details>
<summary>
<em>10/24: Fixed website</em>
</summary>
Some links to sheets have been broken this week. This should be fixed now.
Also, you can now find all true-or-false questions on this website too!
</details>
<details>
<summary>
<em>10/28: Corrected version of sheet 2</em>
</summary>
There were two errors on sheet 2.
One either has to assume that \(k\) is algebraically closed, or work with \(\mathbb{A}^n_k(K)\) for the exercises to work (with \(\bar{k} \subseteq K\)).
Moreover, in the definition of sober, uniqueness of the generic point was missing.
My apologies!
</details>
<details>
<summary>
<em>11/05: Substitution next week.</em>
</summary>
I'm out of town next week.
Someone else will give the tutorial, but it should happen at the same time/place.
Should there be any issues/questions, feel free to send me an e-mail (as always).
</details>
<details>
<summary>
<em>11/14: Fixed Website, ii </em>
</summary>
The link to sheet 5 was broken, but this should be fixed now.
My apologies; see you next week:).
</details>
<details>
<summary>
<em>12/03: no true-or-false questions this week </em>
</summary>
Unfortunately, there will be no true-or-false questions this week.
Normal service will resume next week.
</details>

---

**previous teaching**

summer 2024: [commutative algebra](ca24)

[ag1]: https://www.uni-frankfurt.de/155664843/Algebraische_Geometrie1__WS_2024_25
[olat]: https://olat-ce.server.uni-frankfurt.de/olat/auth/RepositoryEntry/22627549184


[0]: /alggeo2425/sheet0.pdf
[0.5]: /alggeo2425/sheet0.5.pdf
[1]: /alggeo2425/sheet1.pdf
[2]: /alggeo2425/sheet2.pdf
[3]: /alggeo2425/sheet3.pdf
[4]: /alggeo2425/sheet4.pdf
[5]: /alggeo2425/sheet5.pdf
[6]: /alggeo2425/sheet6.pdf
[7]: /alggeo2425/sheet7.pdf
[8]: /alggeo2425/sheet8.pdf
[9]: /alggeo2425/sheet9.pdf
[10]: /alggeo2425/sheet10.pdf
[11]: /alggeo2425/sheet11.pdf
[12]: /alggeo2425/sheet12.pdf
[13]: /alggeo2425/sheet13.pdf
[tf]: /alggeo2425/wahr-oder-falsch-alle.pdf

