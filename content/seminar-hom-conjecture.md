+++
title = "Gaus AG on the Hom-Conjecture"
template = "misc.html"
draft = false 
+++

In this seminar, we try to understand Faltings' exposition of Mochizuki's proof of the Hom-conjecture.
The main reference we will be using Faltings' article [Curves and their fundamental groups][f1]; more references are listed in the program.

Organizers: Alexander Schmidt, Jakob Stix; Program proposal: Magnus Carlson, Ruth Wild

We meet on several Thursdays, from 14 - 18, alternating locations between Frankfurt and Heidelberg.

The precise dates are:

<table cellpadding="3pt" cellspacing="0"> 

<tr> 
<td> October </td> 
<td> 31 </td> 
<td> Frankfurt </td> 
</tr>

<tr>
<td> November</td> 
<td> 21 </td> 
<td> Heidelberg</td> 
</tr>

<tr>
<td> December</td> 
<td> 19 </td> 
<td> Frankfurt</td> 
</tr>

<tr>
<td> January</td> 
<td> 16 </td> 
<td> Heidelberg </td> 
</tr>

<tr>
<td> </td> 
<td> 30 </td> 
<td> Frankfurt </td> 
</tr>

<tr>
<td> February </td> 
<td> 06 </td> 
<td> Heidelberg </td> 
</tr>
</table>

Location Frankfurt: Room 309, Robert-Mayer Straße 6-10 ([📍][frank-loc])

Location Heidelberg: Hörsaal, Mathematikon, Im Neuenheimer Feld 205 ([📍][heid-loc])
 
[Program][program]

---

**Schedule**

(updated on 11/20)

<table cellpadding="3pt" cellspacing="0"> 



<tr>
<td > I </td>
<td > Oct 31 </td>
<td > 1 </td>
<td > Jakob </td>
<td > Introduction </td>
<td >  </td>
</tr>

<tr>
<td >  </td>
<td >  </td>
<td > 2 </td>
<td > Jon </td>
<td > Galois Cohomology and Hodge-Tate decomposition </td>
<td >  </td>
</tr>

<tr>
<td > II </td>
<td > Nov 21 </td>
<td > 3 </td>
<td > Leonie </td>
<td > Unipotent Tannakian categories </td>
<td >  </td>
</tr>

<tr>
<td >  </td>
<td >  </td>
<td > ~ </td>
<td > ~</td>
<td > postponed </td>
<td >  </td>
</tr>

<tr>
<td > III </td>
<td > Dec 19 </td>
<td > 4 </td>
<td > Marius </td>
<td > Construction of h and Hodge-Tateness of rational sections </td>
<td > <a href="/hom-conjecture/t4-marius.pdf">  .pdf </a></td>
</tr>

<tr>
<td > </td>
<td > </td>
<td > 5 </td>
<td > Morten </td>
<td > Bloch-Kato Selmer groups </td>
<td >  </td>
</tr>

<tr>
<td > IV </td>
<td > Jan 16 </td>
<td > 6 </td>
<td > Ruth </td>
<td > Hodge-Tate sections are geometric up to torsion </td>
<td >  </td>
</tr>

<tr>
<td > </td>
<td > </td>
<td > 7 </td>
<td > Benjamin </td>
<td > p-divisible groups </td>
<td >  </td>
</tr>

<tr>
<td > V </td>
<td > Jan 30 </td>
<td > 8 </td>
<td > Amine </td>
<td > Tate-Conjecture </td>
<td >  </td>
</tr>

<tr>
<td > </td>
<td > </td>
<td > 9 </td>
<td > Magnus </td>
<td > Sections geometric up to torsion </td>
<td >  </td>
</tr>

<tr>
<td > VI  </td>
<td > Feb 06 </td>
<td > 10 </td>
<td > Tim </td>
<td > Proof of main result </td>
<td >  </td>
</tr>
</table>

---

**further references that elaborate on parts of the proof**

- [The local pro-p anabelian geometry of curves][moc], Mochizuki (1999)

- [The Grothendieck Conjecture on the Fundamental Groups of Algebraic Curves][ntm], Nakamura, Tamagawa, and Mochizuki (2001)
 
- [Anabelian geometry of hyperbolic curves][msri], Mochizuki (2003)

[f1]: https://www.numdam.org/item/SB_1997-1998__40__131_0.pdf
[program]: /program-hom-conjecture-seminar.pdf
[frank-loc]: https://www.openstreetmap.org/node/795742764
[heid-loc]: https://www.openstreetmap.org/relation/5762624
[moc]: https://link.springer.com/article/10.1007/s002220050381
[ntm]: https://www.kurims.kyoto-u.ac.jp/~motizuki/The%20Grothendieck%20Conjecture%20on%20the%20Fundamental%20Groups%20of%20Algebraic%20Curves.pdf
[msri]: https://library2.msri.org/books/Book41/files/mochizuki.pdf
