+++
title     = "misc"
template  = "misc.html"
+++

this is my (tentative) timetable for next term (winter 24/25):
<center>

 ~ | Mo                | Tue             | Wed             | Thu                | Fri             |
---| ------------------|-----------------|-----------------|----------------    |-----------------|
8  |                   |                 |                 |                    |                 |
10 |                   |                 |                 |                    |                 |
12 |                   |                 |                 |                    |                 |
2  |                   |                 |                 |                    |                 |
4  |                   | Tutorial        |                 |                    |                 |
6  |                   |                 |                 |                    |                 |


</center>


other useful resources:

- the full [GAUS-calendar][gc]

---

- if you're looking for copies of Faltings' paper "Crystalline Cohomology of Semistable Curves -- The Q_p-case" or "Mathematics around Kim's new proof of Siegel's theorem", feel free to reach out
---

the picture on the front page is by Barbara Kroll.

[g1]: https://crc326gaus.de/wp-content/uploads/2024/03/GAUS-AG-SoSe2024-ChromaticHomotopyTheory_program-2024-03-26.pdf
[g2]: https://crc326gaus.de/wp-content/uploads/2024/04/Gaus-AG-SoSe2024-Petrov-programme-2024-04-17.pdf
[gc]: https://crc326gaus.de/events/
[g1zoom]: https://www.wolframalpha.com/input?i=largest+six+digit+prime
[g1paper]: https://arxiv.org/pdf/2402.00960.pdf
[g2paper]: https://arxiv.org/pdf/2109.09301.pdf

