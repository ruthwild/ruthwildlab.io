+++
template  = "index.html"
+++

![image alt <](/me.JPG)
hi!
i am ruth. i am a phd student at frankfurt university under the supervision of jakob stix.
roughly, i am thinking about \\(p\\)-adic methods in anabelian geometry.

this term, magnus carlson and i are organizing a seminar on <a href="/seminar-hom-conjecture" style="color:#d33682;"> mochizuki's proof of the hom-conjecture, d'après faltings.</a>

